package com.example.expandabletest;
public class RecyclerItem {
    private String name;

    public RecyclerItem (String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }
}